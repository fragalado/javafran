package fp.CableTV;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import fp.common.Genero;
import fp.common.OwnHome;
import fp.common.Subscribe;

/**
 * FactoriaCableTV
 */
public class FactoriaCableTV {

    public static List<CableTV> leerCableTV(String rutaFichero){
        //
        List<CableTV> res = new ArrayList<CableTV>();
        List<String> aux = null;
		try {
			aux = Files.readAllLines(Paths.get(rutaFichero));
		} catch (IOException e) {
			e.printStackTrace();
		}

        // Código para saltar la primera línea del csv
        int cont = 0;
        for (String e : aux) {
            if(cont>0){
                CableTV c = parseaCableTV(e);
                res.add(c);
            }
            cont++;
        }
        return res;
    }

    private static CableTV parseaCableTV(String linea){       
        // "47,Male,494828.104378022,2,ownNo,subNo,Suburb mix,22/12/2021,FALSE"
        //  
        String[] trozos = linea.split(",");

        Integer edad = Integer.valueOf(trozos[0].trim());
        Genero genero = Genero.valueOf(trozos[1].trim().toUpperCase());
        Double ingreso = Double.valueOf(trozos[2].trim());
        Integer niños = Integer.valueOf(trozos[3].trim());
        OwnHome ownHome = OwnHome.valueOf(trozos[4].trim().toUpperCase());
        Subscribe subscribe = Subscribe.valueOf(trozos[5].trim().toUpperCase());
        String segment = trozos[6];
        
        String[] fechas = trozos[7].split("/");
        Integer dia = Integer.valueOf(fechas[0]);
        Integer mes = Integer.valueOf(fechas[1]);
        Integer año = Integer.valueOf(fechas[2]);

        LocalDate fecha = LocalDate.of(año, mes, dia);
        Boolean booleano = Boolean.valueOf(trozos[8].trim());


        return new CableTV(edad, genero, ingreso, niños, ownHome, subscribe, segment, fecha, booleano);
        
    }

    // Método para tercer constructor
    public static ContenedorCableTV leerCableTV1(String rutaFichero){
        ContenedorCableTV res = null;
        try {
            Stream<CableTV> sc = Files.lines(Paths.get(rutaFichero))
                            .skip(1)
                            .map(FactoriaCableTV::parseaCableTV);
            res = new ContenedorCableTV("Fran", 1918, sc);
        } catch (IOException e) {
            //
            System.out.println("No se ha encontrado el fichero " + rutaFichero);
            e.printStackTrace();
        }
        return res;
    }
}