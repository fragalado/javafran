package fp.CableTV;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import fp.common.Genero;
import fp.common.OwnHome;

// TIPO CONTENEDOR
public class ContenedorCableTV {
    //
    // ATRIBUTOS

    private String nombre;
    private  Integer anyo;
    private List<CableTV> cables;

    
    // CONSTRUCTORES
    // Constructor con todas las propiedades básicas excepto la colección
    public ContenedorCableTV(String nombre, Integer anyo){
        this.nombre = nombre;
        this.anyo = anyo;
        this.cables = new ArrayList<CableTV>();
    }

    // Constructor con todas las propiedades básicas y la colección
    public ContenedorCableTV(String nombre, Integer anyo ,List<CableTV> cables) {
        this.nombre = nombre;
        this.anyo = anyo;
        this.cables = new ArrayList<CableTV>(cables);
    }

    // Constructor con todas las propiedades básicas excepto la colección y un stream del tipo base
    public ContenedorCableTV(String nombre, Integer anyo, Stream<CableTV> cables){
        this.nombre = nombre;
        this.anyo = anyo;
        this.cables = cables.collect(Collectors.toList());
    }

    // GETTERS
    public String getNombre() {
        return nombre;
    }

    public Integer getAnyo() {
        return anyo;
    }

    public List<CableTV> getCables() {
        return new ArrayList<CableTV>(this.cables);
    }

    // OPERACIONES
    // a) Obtener el número de elementos
    public Integer numeroElementos() {
        // 
        int res = this.cables.size();
        return res;
    }

    // b) Añadir un elemento
    public void añadirElemento(CableTV c) {
        this.cables.add(c);
    }

    // c) Añadir una colección de elementos
    public void añadirElementos(List<CableTV> d) {
        this.cables.addAll(d);
    }

    // d) Eliminar un elemento
    public void eliminarElemento(CableTV c){
        // 
        this.cables.remove(c);
    }

    // 1) Existe
    public Boolean existeElementoConMasAñosQueLosIndicados(Integer a) {
        //
        Boolean res = false;
        for (CableTV e : cables) {
            //
            if (e.getEdad() > a) {
                //
                res = true;
                break;
            }
        }
        return res;
    }
    

    // 2) Contador
    public Integer contadorOwnYes() {
        int res = 0;
        for (CableTV e : cables) {
            //
            if (e.getOwnHome().equals(OwnHome.OWNYES)) {
                res++;
            }
        }

        return res;
    }

    // 3) Filtrado
    public Set<String> getSegment(){
        Set<String> res = new HashSet<>();
        for (CableTV e : this.cables) {
            res.add(e.getSegment());
        }
        return res;
    }

    // 4) Map, Método de agrupación
    public Map<Genero, List<CableTV>> metodoAgrupacion(){
        Map<Genero, List<CableTV>> res = new HashMap<>();
        for (CableTV e : this.cables) {
            Genero clave = e.getGenero();
            if (!res.containsKey(clave)) {
                // Primera vez
                List<CableTV> aux = new ArrayList<>();
                res.put(clave, aux);

            } else {
                // Actualizando
                res.get(clave).add(e);
            }
        }
        return res;
    }

    // 5) Map, Método de acumulación
    public Map<Genero, Integer> metodoAcumulacion(){
        Map<Genero, Integer> res = new HashMap<>();
        for (CableTV e : this.cables) {
            Genero clave = e.getGenero();
            if (!res.containsKey(clave)) {
                // Primera vez
                res.put(clave, 1);
            } else {
                // Actualizando
                res.put(clave, res.get(clave) + 1);
            }
        }

        return res;
    }

    // 6) Existe, con Stream
    public Boolean existeElementoConMasAñosQueLosIndicadosConStream(Integer a){
        return this.cables.stream()
                .anyMatch(x-> x.getEdad() > a);
    }

    // 7) Contador, con Stream
    public Long contadorOwnYesConStream(){
        return this.cables.stream()
                .filter(x-> x.getOwnHome().equals(OwnHome.OWNYES))
                .count();
                
    }

    // 8) Filtrado, con Stream
    public Set<String> getSegmentConStream(){
        return this.cables.stream()
                .map(CableTV::getSegment)
                .collect(Collectors.toSet());
    }

    // 9) Máximo con Stream
    public Integer maximaEdad(){
        return this.cables.stream()
                .mapToInt(CableTV::getEdad)
                .max()
                .getAsInt();
    }

    // 10) Filtrado y ordenación
    public List<CableTV> filtradoHombreYOrdenacionEdad(){
        return this.cables.stream()
                .filter(x-> x.getGenero().equals(Genero.MALE))
                .sorted(Comparator.comparing(CableTV::getEdad))   // Ordenado de menor a mayor edad, si lo queremos al reves 
                .collect(Collectors.toList());                    // añadimos ...(CableTV::getEdad).reversed()

    }

    // 11) Métodos 4) y 5) pero con streams
    public Map<Genero, List<CableTV>> metodoAgrupacionConStream(){
        return this.cables.stream()
                .collect(Collectors.groupingBy(CableTV::getGenero));
    }

    public Map<Genero, Integer> metodoAcumulacionConStream(){
        return this.cables.stream()
                .collect(Collectors.groupingBy(CableTV::getGenero, 
                            Collectors.collectingAndThen(Collectors.counting(), x-> x.intValue())));
    }

    // 12) Usando mapping
    public Map<Genero, List<Double>> getMapaGeneroAsociadoAListaDeIngresos(){
        return this.cables.stream()
                .collect(Collectors.groupingBy(CableTV::getGenero, 
                            Collectors.mapping(CableTV::getIngreso, Collectors.toList())));
    }

    // 12) Usando collectingAndThen
    public Map<Genero, Double> getGeneroAsociadoAIngresoTotal(){
        return this.cables.stream()
                .collect(Collectors.groupingBy(CableTV::getGenero,
                            Collectors.summingDouble(CableTV::getIngreso)));
    }

    // 13) Map con maximo y minimo
    public Map<Genero, CableTV> getGeneroAsociadoAValorIngreso(){
        return this.cables.stream()
                .collect(Collectors.groupingBy(CableTV::getGenero,
                                Collectors.collectingAndThen(Collectors.maxBy(Comparator.comparing(CableTV::getIngreso)), o-> o.get())));
    }

    // 14) SortedMap con valor una lista
    public SortedMap<Genero, List<Double>> getGeneroAsociadoAListaConNMejoresIngresos(Integer n){
        
        //
        return this.cables.stream()
                .collect(Collectors.groupingBy(CableTV::getGenero, TreeMap::new,
                            Collectors.collectingAndThen(Collectors.toList(), l -> calcularNMejoresIngresos(l, n))));
    }

    private static List<Double> calcularNMejoresIngresos(List<CableTV> cables, Integer n){
        return cables.stream()
                .sorted(Comparator.comparing(CableTV::getIngreso).reversed())
                .map(x-> x.getIngreso())
                .limit(n)
                .collect(Collectors.toList());

    }
    // 15) Map devuelve valor asociado mayor de todo el Map
    public CableTV getValorMaximoDeTodoElMap(){
        //
        Map<Genero, CableTV> m = this.cables.stream()
        .collect(Collectors.groupingBy(CableTV::getGenero,
                        Collectors.collectingAndThen(Collectors.maxBy(Comparator.comparing(CableTV::getIngreso)), o-> o.get())));
        
        CableTV res = m.entrySet().stream()
                    .max(Comparator.comparing(l -> l.getValue()))
                    .get()
                    .getValue();
        return res;
       
    }



    // MÉTODOS ADICIONALES
    // a) Representación como cadena (toString)
    @Override
    public String toString() {
        return "ContenedorCableTV [anyo=" + anyo + ", numeroElementos=" + numeroElementos() + ", nombre=" + nombre + "]";
    }

    // b) Criterio de igualdad (hashCode y equals)
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((anyo == null) ? 0 : anyo.hashCode());
        result = prime * result + ((cables == null) ? 0 : cables.hashCode());
        result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ContenedorCableTV other = (ContenedorCableTV) obj;
        if (anyo == null) {
            if (other.anyo != null)
                return false;
        } else if (!anyo.equals(other.anyo))
            return false;
        if (cables == null) {
            if (other.cables != null)
                return false;
        } else if (!cables.equals(other.cables))
            return false;
        if (nombre == null) {
            if (other.nombre != null)
                return false;
        } else if (!nombre.equals(other.nombre))
            return false;
        return true;
    }    

}
