package fp.CableTV.test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fp.CableTV.CableTV;
import fp.common.Genero;
import fp.common.OwnHome;
import fp.common.Subscribe;

public class TestCableTV {
    public static void main(String[] args) {
        LocalDate f1 = LocalDate.of(2021, 06, 13);
        LocalDate f2 = LocalDate.of(2020, 04, 21);
        List<LocalDate> fechas = new ArrayList<LocalDate>();
        fechas.add(f1);
        fechas.add(f2);

        CableTV c1 = new CableTV(41, Genero.MALE, 42141.421, 3, OwnHome.OWNNO, Subscribe.SUBYES, "Suburb mix", LocalDate.of(2021, 12, 28), true);
        System.out.println(c1);

        // Prueba de Get
        System.out.println("Año: " + c1.getEdad());
        System.out.println("Genero: " + c1.getGenero());
        System.out.println("Ingresos: "+ c1.getIngreso());
        System.out.println("Niños: " + c1.getNiños());
        System.out.println("ownHome: " + c1.getOwnHome());
        
        System.out.println("ownHome2: " +c1.convertirOwnHome(c1.getOwnHome()));

        System.out.println("Subscribe: " + c1.getSubscribe());
        System.out.println("Segment : " + c1.getSegment());
        System.out.println("Fecha: " + c1.getFecha());
        System.out.println("Booleano : " + c1.isBooleano());
        System.out.println("Lista fechas: " + c1.getFechas());

        // Prueba de Set
        c1.setEdad(12);
        c1.setBooleano(false);
        c1.setFecha(LocalDate.of(2020, 06, 12));
        c1.setGenero(Genero.FEMALE);
        c1.setIngreso(1212.53);
        c1.setNiños(1);
        c1.setOwnHome(OwnHome.OWNYES);
        c1.setSegment("Travelers");
        c1.setSubscribe(Subscribe.SUBNO);
        System.out.println(c1);

        // Prueba método getFormatoCorto
        System.out.println(c1.getFormatoCorto());


        // Prueba de Checker
        // CableTV c2 = new CableTV(41, Genero.MALE, 42141.421, -1, OwnHome.OWNNO, Subscribe.SUBYES, "Suburb mix", LocalDate.of(2020, 12, 28), true, fechas);
        // CableTV c3 = new CableTV(41, Genero.MALE, 42141.421, 3, OwnHome.OWNNO, Subscribe.SUBYES, "Suburb mix", LocalDate.of(2018, 12, 28), true, fechas);
        


        
        

       
        
    }
}
