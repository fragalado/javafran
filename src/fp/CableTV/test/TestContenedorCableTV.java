package fp.CableTV.test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fp.CableTV.CableTV;
import fp.CableTV.ContenedorCableTV;
import fp.CableTV.FactoriaCableTV;
import fp.common.Genero;
import fp.common.OwnHome;
import fp.common.Subscribe;

public class TestContenedorCableTV {
    //
    public static void main(String[] args) {

        List<CableTV> lista = FactoriaCableTV.leerCableTV("./data/CableTVSubscribersData.csv");
        
        CableTV c1 = new CableTV(10, 412421.412, 0, OwnHome.OWNNO, "Suburb mix", 
                LocalDate.of(2021, 11, 29));


        CableTV c2 = new CableTV(34, Genero.MALE, 53674.931370168, 5, OwnHome.OWNYES, Subscribe.SUBNO, 
                "Moving up", LocalDate.of(2021, 11, 25), false);

        ContenedorCableTV r = new ContenedorCableTV("Francisco", 2021, lista);


        System.out.println("El número total de elementos es : " + r.numeroElementos());

        r.añadirElemento(c1);
        System.out.println("El número total de elementos después de añadir un elemento es : " + r.numeroElementos());

        r.eliminarElemento(c1);
        System.out.println("El número total de elementos después de quitar un elemento es : " + r.numeroElementos());

        List<CableTV> c3 = new ArrayList<>();
        c3.add(c2);
        r.añadirElementos(c3);
        System.out.println("El número total de elementos después de añadir una lista de elementos es : " + r.numeroElementos());
        
        System.out.println("\nMétodo existe elemento : " + r.existeElementoConMasAñosQueLosIndicados(40));
        System.out.println("\nMétodo existe elemento 2 : " + r.existeElementoConMasAñosQueLosIndicados(81));

        System.out.println("\nMétodo contador : " + r.contadorOwnYes());

        System.out.println("\nMétodo filtrado : " + r.getSegment());

        System.out.println("\nMétodo mapa agrupación : " + r.metodoAgrupacion());

        System.out.println("\nMétodo mapa acumulación : " + r.metodoAcumulacion());


        // Métodos para stream
        System.out.println("\nMétodo existe con stream : " + r.existeElementoConMasAñosQueLosIndicadosConStream(40));
        System.out.println("\nMétodo existe con stream : " + r.existeElementoConMasAñosQueLosIndicadosConStream(81));

        System.out.println("\nMétodo contador con stream : " + r.contadorOwnYesConStream());

        System.out.println("\nMétodo filtrado con stream : " + r.getSegmentConStream());

        System.out.println("\nMétodo máximo con stream : " + r.maximaEdad());

        System.out.println("\nMétodo filtrado y ordenación: " + r.filtradoHombreYOrdenacionEdad());

        System.out.println("\nMétodo 4 pero con stream : " + r.metodoAgrupacionConStream());
        
        System.out.println("\nMétodo 5 pero con stream : " + r.metodoAcumulacionConStream());
        
        System.out.println("\nMétodo usando map con mapping : " + r.getMapaGeneroAsociadoAListaDeIngresos());

        System.out.println("\nMétodo usando map con collectingAndThen : " + r.getGeneroAsociadoAIngresoTotal());
        
        System.out.println("\nMétodo usando map con máximo : " + r.getGeneroAsociadoAValorIngreso());

        System.out.println("\nMétodo usando sortedmap : " + r.getGeneroAsociadoAListaConNMejoresIngresos(4));

        System.out.println("\nMétodo 15) : " + r.getValorMaximoDeTodoElMap());
        

        
    }
}
