package fp.CableTV.test;

import java.util.List;

import fp.CableTV.CableTV;
import fp.CableTV.FactoriaCableTV;

public class TestFactoriaCableTV {
    //
    public static void main(String[] args) {
        String ruta = "./data/CableTVSubscribersData.csv"; // data/CableTVSubscribersData.csv
        List<CableTV> lista = FactoriaCableTV.leerCableTV(ruta);
        for (CableTV e : lista) {
            System.out.println(e);
        }
    }
}
