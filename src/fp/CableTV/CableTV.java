package fp.CableTV;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fp.common.Genero;
import fp.common.OwnHome;
import fp.common.Subscribe;
import fp.utiles.Checkers;

/**
 * CableTV
 */
public class CableTV implements Comparable<CableTV>{

    // Atributos
    private int edad;
    private Genero genero;
    private Double ingreso;
    private int niños;
    private OwnHome ownHome;
    private Subscribe subscribe;
    private String segment;
    private LocalDate fecha;
    private boolean booleano;
    private List<LocalDate> fechas;
    
    // Constructores
    // C1: Tiene un parámetro por cada propiedad básica del tipo:
    public CableTV(int edad, Genero genero, Double ingreso, int niños, OwnHome ownHome, Subscribe subscribe, String segment,
            LocalDate fecha, boolean booleano) {
        this.edad = edad;
        this.genero = genero;
        this.ingreso = ingreso;
        // R1: El número de niños no puede ser menor a 0
        Checkers.check("El numero de niños no puede ser negativo", niños >= 0);
        this.niños = niños;
        this.ownHome = ownHome;
        this.subscribe = subscribe;
        this.segment = segment;
        // R2: El año tiene que ser posterior a 2019
        Checkers.check("El año tiene que ser posterior a 2019", fecha.getYear() > 2019);
        this.fecha = fecha;
        this.booleano = booleano;
        //
        this.fechas = new ArrayList<>();
        this.fechas.add(fecha);
    }
    
    /* C2:
        Crea un objeto de tipo CableTV a partir de los siguientes parámetros: int edad, Double ingreso, 
        int niños, OwnHome ownHome, String segment, LocalDate fecha, List<LocalDate> fechas :
    */
    public CableTV(int edad, Double ingreso, int niños, OwnHome ownHome, String segment, LocalDate fecha) {
        this.edad = edad;
        this.genero = Genero.MALE;
        this.ingreso = ingreso;
        // R1: El número de niños no puede ser menor a 0
        Checkers.check("El numero de niños no puede ser negativo", niños >= 0);
        this.niños = niños;
        this.ownHome = ownHome;
        this.subscribe = Subscribe.SUBYES;
        this.segment = segment;
        // R2: El año tiene que ser posterior a 2019
        Checkers.check("El año tiene que ser posterior a 2019", fecha.getYear() > 2019);
        this.fecha = fecha;
        this.booleano = true;
        //
        this.fechas = new ArrayList<>();
        this.fechas.add(fecha);
    } 


    // Métodos de las propiedades. Getter and Setter
    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }


    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }


    public Double getIngreso() {
        return ingreso;
    }

    public void setIngreso(Double ingreso) {
        this.ingreso = ingreso;
    }


    public int getNiños() {
        return niños;
    }

    public void setNiños(int niños) {
        // R1: El número de niños no puede ser menor a 0
        Checkers.check("El numero de niños no puede ser negativo", niños >= 0);
        this.niños = niños;
    }


    public OwnHome getOwnHome() {
        return ownHome;
    }

    public void setOwnHome(OwnHome ownHome) {
        this.ownHome = ownHome;
    }


    public Subscribe getSubscribe() {
        return subscribe;
    }

    public void setSubscribe(Subscribe subscribe) {
        this.subscribe = subscribe;
    }


    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }


    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        // R2: El año tiene que ser posterior a 2019
        Checkers.check("El año tiene que ser posterior a 2019", fecha.getYear() > 2019);
        this.fecha = fecha;
    }


    public boolean isBooleano() {
        return booleano;
    }

    public void setBooleano(boolean booleano) {
        this.booleano = booleano;
    }

    
    public List<LocalDate> getFechas() {
        List<LocalDate> fechaS = new ArrayList<>();
        fechaS.addAll(this.fechas);
        return fechaS;
    }

    public void setFechas(List<LocalDate> f) {
        this.fechas = f;
    }

    // Métodos derivados
    // Convierte el valor del enumerado OwnHome en "Yes" o "No" dependiendo de su valor:
    public String convertirOwnHome(OwnHome p) {
        String r = null;
        switch (p) {
            case OWNYES:
                r = "Yes";
                break;
        
            default:
                r = "No";
                break;
        }
        
        return r;
    }

    // Devuelve una cadena con la fecha, niños, ingreso y género:
    public String getFormatoCorto(){
        return this.fecha + " ---> (" + this.niños + ", "+ this.ingreso + ", " + this.genero + ")";
    }

    // Métodos adicionales
    // a) Representación como cadena
    @Override
    public String toString() {
        return "CableTV [edad=" + edad + ", booleano=" + booleano + ", fecha=" + fecha + ", fechas=" + fechas
                + ", genero=" + genero + ", ingreso=" + ingreso + ", niños=" + niños + ", ownHome=" + convertirOwnHome(ownHome)
                + ", segment=" + segment + ", subscribe=" + subscribe + "]";
    }
    
    // b) Criterio de igualdad (hashCode y equals)
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ownHome == null) ? 0 : ownHome.hashCode());
        result = prime * result + ((segment == null) ? 0 : segment.hashCode());
        result = prime * result + ((subscribe == null) ? 0 : subscribe.hashCode());
        return result;
    }

    // Son iguales si lo son todas sus propiedades:
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CableTV other = (CableTV) obj;
        if (ownHome != other.ownHome)
            return false;
        if (segment == null) {
            if (other.segment != null)
                return false;
        } else if (!segment.equals(other.segment))
            return false;
        if (subscribe != other.subscribe)
            return false;
        return true;
    } 

    // c) Criterio de ordenación (compareTo)
    // Se ordena por segmento, subscripción y ownHome, en este orden.
    @Override
    public int compareTo(CableTV o) {
        // 
        int r = this.getSegment().compareTo(o.getSegment());
        if(r == 0){
            r = this.getSubscribe().compareTo(o.getSubscribe());
            if(r == 0){
                r = this.getOwnHome().compareTo(o.getOwnHome());
            }
        }

        return r;
    }

    

    
}   