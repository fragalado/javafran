# Proyecto del Segundo Cuatrimestre Fundamentos de Programación (Curso  \XX\/\YY\)
Autor/a: Francisco José Gallego Dorado   uvus: fragaldor

Aquí debes añadir la descripción del dataset y un enunciado del dominio del proyecto.


## Estructura de las carpetas del proyecto

* **/src**: Contiene los diferentes archivos que forman parte del proyecto. Debe estar estructurado en los siguentes paquetes
  * **fp.CableTV**: Paquete que contiene los tipos del proyecto.
  * **fp.CableTV.test**: Paquete que contiene las clases de test del proyecto.
  * **fp.common**: Paquete que contiene los tipos auxiliares del proyecto
  * **fp.utiles**:  Paquete que contiene las clases de utilidad. 
* **/data**: Contiene el dataset o datasets del proyecto
    * **CableTVSubscribersData.csv**: Contiene información sobre las personas que se han subscrito a un determinado tipo de servicio de televisión por cable.
    
## Estructura del *dataset*

Aquí debes describir la estructura del dataset explicando qué representan los datos que contiene y la descripción de cada una de las columnas. Incluye también la URL del dataset original.

El dataset está compuesto por 9 columnas, con la siguiente descripción:

* **columna 1**: de tipo Integer, representa los años de los suscriptores.
* **columna 2**: de tipo Genero, representa el género de los suscriptores.
* **columna 3**: de tipo Double, representa el ingreso de los suscriptores.
* **columna 4**: de tipo Integer, representa el número de niños que tiene el suscriptor. 
* **columna 5**: de tipo OwnHome, representa si el suscriptor es dueño de la casa o no.
* **columna 6**: de tipo Subscribe, representa si se han subscrito a los servicios de TV o no.
* **columna 7**: de tipo String, representa el segmento de la suscripción del suscriptor.
* **columna 8**: de tipo LocalDate
* **columna 9**: de tipo Boolean

## Tipos implementados

Los tipos que se han implementado en el proyecto son los siguientes:

### Tipo Base - CableTV
Representa la información de las personas que se han suscrito a un determinado tipo de servicio de televisión por cable.

**Propiedades**:

- año, de tipo Integer, consultable y modificable. Representa los años de los suscriptores.
- genero, de tipo Genero, consultable y modificable. Representa el género de los suscriptores.
- ingreso, de tipo Double, consultable y modificable. Representa el ingreso de los suscriptores.
- niños, de tipo Integer, consultable y modificable. Representa el número de niños que tiene el suscriptor. 
- ownHome, de tipo OwnHome, consultable y modificable. Representa si el suscriptor es dueño de la casa o no.
- subscribe, de tipo Subscribe, consultable y modificable. Representa si se han subscrito a los servicios de TV o no.
- segment, de tipo String, consultable y modificable. Representa el segmento de la suscripción del suscriptor.
- fecha, de tipo LocalDate, consultable y modificable. Representa una fecha
- booleano, de tipo Boolean, consultable y modificable. 
- fechas, de tipo List<LocalDate>, consultable y modificable. Representa una lista con fechas.

**Constructores**: 

- C1: Tiene un parámetro por cada propiedad básica del tipo.
- C2: Crea un objeto de tipo CableTV a partir de los siguientes parámetros: int año, Double ingreso, int niños, OwnHome ownHome, String segment, LocalDate fecha, List<LocalDate> fechas .

**Restricciones**:
 
- R1: El número de niños no puede ser menor a 0.
- R2: El año tiene que ser posterior a 2019.

**Criterio de igualdad**: Son iguales si lo son sus propiedades.

**Criterio de ordenación**: Por segmento, subscripción y ownHome. En ese orden.

**Otras operaciones**:
 
-	String convertirOwnHome(OwnHome p): Convierte el valor del enumerado OwnHome en "Yes" o "No" dependiendo de su valor.
- String getFormatoCorto(): Devuelve una cadena con la fecha, niños, ingreso y género.

#### Tipos auxiliares
- Genero, enumerado. Puede tomar los valores MALE, FEMALE.
- OwnHome, enumerado. Puede tomar los valores OWNYES, OWNNO.
- Subscribe, enumerado. Puede tomar los valores SUBYES, SUBNO.

### Factoría
Descripción breve de la factoría.

- leerCableTV: Devuelve una lista del tipo base, List<CableTV> se le pasa por parámetro la ruta del fichero y lo que hace es leer el csv.
-	parseaCableTV: Devuelve el tipo base, CableTV, y lo que hace es pasar de String a las propiedades necesarias.


### Tipo Contenedor

Descripción breve del tipo contenedor.

**Propiedades**:

- nombre, de tipo String, consultable. 
- anyo, de tipo Integer, consultable. 
- cables, de tipo List<CableTV>, consultable.

**Constructores**: 

- C1: Constructor con todas las propiedades básicas excepto la colección.
- C2: Constructor con todas las propiedades básicas y la colección.
- C3: Constructor con todas las propiedades básicas excepto la colección, además tiene un stream del tipo base.

**Restricciones**:
 
- No tiene ninguna restricción.

**Criterio de igualdad**: Son iguales si lo son sus propiedades.

**Criterio de ordenación**: No tiene criterio de ordenación.

**Otras operaciones**:
 
-	numeroElementos: Cuenta todos los elementos que hay en el csv.
- añadirElemento: Añade un elemento del tipo base.
- añadirElementos: Añade una lista de elementos del tipo base.
- eliminarElemento: Elimina un elemento del tipo base.
- existeElementoConMasAñosQueLosIndicados: Devuelve un boolean que devuelve true si hay alguién con más años que el pasado por parámetro
- contadorOwnYes: Cuenta todos los OwnYes (propiedad del tipo base, OWNHOME) que hay en el csv.
- getSegment: Filtra por Segment (propiedad del tipo base).
- metodoAgrupación: Devuelve un mapa de clave Genero y de valor una lista del tipo base.
- metodoAcumulación: Devuelve un mapa de clave Genero y de valor el numero total que aparecen.
- existeElementoConMasAñosQueLosIndicadosConStream: Devuelve un boolean que devuelve true si hay alguién con más años que el pasado por parámetro
- contadorOwnYesConStream: Cuenta todos los OwnYes (propiedad del tipo base, OWNHOME) que hay en el csv.
- getSegmentConStream: Filtra por Segment (propiedad del tipo base).
- maximaEdad: Devuelve la máxima edad.
- filtradoHombreYOrdenacionEdad: Filtra por Male y ordena por la edad.
- metodoAgrupacionConStream: Devuelve un mapa de clave Genero y de valor una lista del tipo base.
- metodoAcumulacionConStream: Devuelve un mapa de clave Genero y de valor el numero total que aparecen.
- getMapaGeneroAsociadoAListaDeIngresos: Devuelve un mapa de clave Genero y de valor una lista con todos los ingresos.
- getGeneroAsociadoAIngresoTotal: Devuelve un mapa de clave Genero y de valor la suma de todos los ingresos.
- getGeneroAsociadoAValorIngreso: Devuelve un mapa de clave Genero y de valor el máximo ingreso.
- getGeneroAsociadoAListaConNMejoresIngresos: Devuelve un sortedMap de clave Genero y de valor 
- getValorMaximoDeTodoElMap: Obtiene el valor máximo de ingreso entre hombres y mujeres.
